import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, Select } from '@ngxs/store';
import { User } from '../models/user.models';
import { UsersRequestAttempt, UsersRequestFailure, UsersRequestSuccess } from '../store/user.actions';
import {UserStateModel} from '../store/user.state';
@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {
  
  @Select(state => state.users)
  users$: Observable<UserStateModel>;

  constructor(private store: Store) { }
  

  ngOnInit():void {
    this.store.dispatch(new UsersRequestAttempt());
  }

}
