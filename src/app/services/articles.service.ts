import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from '../models/article.models';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  public API_KEY = '23187997c8b64d6f958d7bcd0d3a1171';

  constructor(private httpClient: HttpClient) {

   }
   getNewsTechCrunch():Observable<Article[]>{
    return this.httpClient.get<Article[]>(`https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=${this.API_KEY}`);
   }
   getNewsGoogle():Observable<any>{
     return this.httpClient.get<any>(`https://newsapi.org/v2/top-headlines?sources=google-news&apiKey=${this.API_KEY}`);
   }
}
