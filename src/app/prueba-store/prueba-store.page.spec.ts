import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebaStorePage } from './prueba-store.page';

describe('PruebaStorePage', () => {
  let component: PruebaStorePage;
  let fixture: ComponentFixture<PruebaStorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PruebaStorePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebaStorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
