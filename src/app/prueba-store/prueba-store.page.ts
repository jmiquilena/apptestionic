import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Increment, Decrement, SetTotal } from '../store/counter.actions';
import { Observable } from 'rxjs';
import {CounterState, CounterStateModel} from '../store/counter.state';
// import { UsersRequestAttempt } from './store/users.actions';
// import { UsersStateModel } from './store/users.state';

@Component({
  selector: 'app-prueba-store',
  templateUrl: './prueba-store.page.html',
  styleUrls: ['./prueba-store.page.scss'],
})
export class PruebaStorePage implements OnInit {
 //seleccionamos el slice counter del estado global
 @Select(state=>state.counter) 
 counter$: Observable<CounterStateModel>

 //Inyectamos la store global en el componente
 constructor(private store: Store) { }

 //Invocamos las acciones del store 
 increment(){
   this.store.dispatch(new Increment());
 }
 decrement(){
   this.store.dispatch(new Decrement());
 }
 reset(){
   this.store.dispatch(new SetTotal(0));
 }

  ngOnInit() {
  }

}
