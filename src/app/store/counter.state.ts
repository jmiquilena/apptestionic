import {State, Store, StateContext, Action} from '@ngxs/store';

import { Increment, Decrement, SetTotal } from './counter.actions';

//creamos un tipo para nuestro estado

export interface CounterStateModel{
    total: number;
}

/**
 * Creamos nuestro estado con la anotacion @State
 * le damos el tipo al estadoi
 * le damos nombre al 'slice o particion del estado
 * damos valor por defecto al estado
 */

 @State({
     name:'counter',
     defaults:{
         total: 0
     }
 })
 export class CounterState {
     //Inyectamos ela store global en nuestro estado
     constructor(private store: Store){}

     //Relacionamos la accion con  su implementaciòn con  la anotación @Action(nombre_de_accion).
     //Inyectamos a la funcion el estado actual con 'stateContext: StateContext'.
     @Action(Increment)
     Increment(stateContext: StateContext<CounterStateModel>){
         //Recogemos el valor actual del total con 'store.getState().nombre_propiedad
         const currentTotal = stateContext.getState().total;
         //Actualizamos el estado con pathState({nombre_propiedad: valor})
         stateContext.patchState({ total: currentTotal + 1});
     }
     @Action(Decrement)
     Decrement(stateContext: StateContext<CounterStateModel>){
         const currentTotal = stateContext.getState().total;
         stateContext.patchState({total: currentTotal - 1});
     }
     //Inyectamos el vaklor de la acción y recuperamos el valor pasado por parámetro
     @Action(SetTotal)
     SetTotal(stateContext: StateContext<CounterStateModel>, action: SetTotal){
         stateContext.patchState({ total: action.value });
     }
 }


