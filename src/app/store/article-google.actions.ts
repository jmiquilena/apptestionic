import { HttpErrorResponse } from '@angular/common/http';
import { Article } from '../models/article.models';

export class ArticleGoogleRequestAttempt{
    static readonly type = '[Article] Request Attempt';
    constructor(){}
}

export class ArticleGoogleRequestSuccess{
    static readonly type='[Article] Request Success';
    constructor(public articlesGoogle: Article[]){}
}
export class ArticleGoogleRequestFailure{
    static readonly type='[Article] Request Failure';
    constructor(public error: HttpErrorResponse){}
}
