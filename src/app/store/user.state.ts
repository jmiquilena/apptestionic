import {State, Store, StateContext, Action} from '@ngxs/store';
import { User } from '../models/user.models';
import {UsersRequestAttempt, UsersRequestSuccess, UsersRequestFailure} from './user.actions';
import { UsersService } from '../services/users.service';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

export interface UserStateModel{
    users: User[];
}

@State({
    name: 'users',
    defaults: {
        users:[]
    }

})

export class UsersState{
    constructor(private store: Store, private userService: UsersService){}

    @Action(UsersRequestAttempt)
    async UsersRequestAttempt(){
        this.userService.getUsers().subscribe(data=>{
            this.store.dispatch(new UsersRequestSuccess(data));
        },
        error=>{
            this.store.dispatch(new UsersRequestFailure(error));
        })
    }

    @Action(UsersRequestSuccess)
    UsersRequestSuccess(
        stateContext : StateContext<UserStateModel>, 
        action: UsersRequestSuccess
    ){
        stateContext.patchState({users: action.users});
    }

    @Action(UsersRequestFailure)
    UsersRequestFailure(
        stateContext: StateContext<UserStateModel>,
        action: UsersRequestFailure
    ){
        console.log('Failed to get Users. Try again later', action.error);
    }

    
}
