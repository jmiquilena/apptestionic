import { State, Store, StateContext, Action } from '@ngxs/store';
import { Article } from '../models/article.models';
import { ArticleGoogleRequestAttempt, ArticleGoogleRequestFailure, ArticleGoogleRequestSuccess } from './article-google.actions';
import { ArticlesService } from '../services/articles.service';
import { throwError } from  'rxjs';
import { catchError } from 'rxjs/operators';

export interface ArticleGoogleStateModel{
    articlesGoogle: Article[];
}

@State<ArticleGoogleStateModel>({
    name:'articlesGoogle',
    defaults:{
        articlesGoogle:[],
    
    }
})
export class ArticlesGoogleState{
    constructor(private store: Store, private articlesService: ArticlesService){}

    @Action(ArticleGoogleRequestAttempt)
    async ArticleGoogleRequestAttempt(){
        this.articlesService.getNewsGoogle().subscribe(data=>{
            console.log(data);
            this.store.dispatch(new ArticleGoogleRequestSuccess(data.articles));
        }, error=>{
            this.store.dispatch(new ArticleGoogleRequestFailure(error));
        })
    }
    @Action(ArticleGoogleRequestSuccess)
    ArticleGoogleRequestSuccess(
        ctx: StateContext<ArticleGoogleStateModel>,
        action: ArticleGoogleRequestSuccess
    ){
        ctx.patchState({articlesGoogle: action.articlesGoogle});
    }
    @Action(ArticleGoogleRequestFailure)
    ArticleGoogleRequestFailure(
        ctx: StateContext<ArticleGoogleStateModel>,
        action: ArticleGoogleRequestFailure
    ){
      console.log('Failed to get News Google. Try again later', action.error);  
    }


}