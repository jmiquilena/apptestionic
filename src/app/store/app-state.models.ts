
import { ArticleGoogleStateModel } from './article-google.state';
import { UserStateModel } from './user.state';

export interface AppStateModel{
    articlesGoogle: ArticleGoogleStateModel;
    users: UserStateModel;
}