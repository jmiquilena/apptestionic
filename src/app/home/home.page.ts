import { Component } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ArticleGoogleStateModel } from '../store/article-google.state';
import { ArticleGoogleRequestAttempt, ArticleGoogleRequestFailure, ArticleGoogleRequestSuccess } from '../store/article-google.actions';
import { AppStateModel } from '../store/app-state.models';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  google: boolean = false;
  techCrunch: boolean = false;

  @Select((state:AppStateModel) => state.articlesGoogle.articlesGoogle)
  articlesGoogle$: Observable<ArticleGoogleStateModel>;

  constructor(private store: Store) {}


  //functions
  ionViewDidTechCrunch(obj:boolean){
    if (obj){

    }
    this.techCrunch=obj;
    
  }

  ionViewDidGoogle(obj:boolean){
    if (obj){
      this.store.dispatch(new ArticleGoogleRequestAttempt());
    }
      this.google=obj;
    }
  


}
