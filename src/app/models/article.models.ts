export interface Article{
    title:string,
    description:string,
    url:string,
    ulrToImage:string
}